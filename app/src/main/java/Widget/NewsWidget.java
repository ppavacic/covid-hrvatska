package Widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.util.Pair;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.ppavacic.covidhr.Common.ParserAsyncTask;
import com.ppavacic.covidhr.Common.WidgetFetchData;
import com.ppavacic.covidhr.Models.NewsViewModel;
import com.ppavacic.covidhr.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Implementation of App Widget functionality.
 */
public class NewsWidget extends AppWidgetProvider {
    private ArrayList<Pair<String, String>> articles = new ArrayList<Pair<String, String>>(){
        {
            add(new Pair<String, String> ("Nema Podataka", "Nema Podataka"));
            add(new Pair<String, String> ("Nema Podataka", "Nema Podataka"));
            add(new Pair<String, String> ("Nema Podataka", "Nema Podataka"));
            add(new Pair<String, String> ("Nema Podataka", "Nema Podataka"));
        }
    };
    private int county = 0;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        county = context.getSharedPreferences(context.getString(R.string.home_county_ind_key), Context.MODE_MULTI_PROCESS).getInt("0", 0);
        fetchArticles(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    private void fetchArticles(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        WidgetFetchData bgTask = new WidgetFetchData(context, appWidgetManager, appWidgetIds, articles, county);
        bgTask.execute();
    }
}