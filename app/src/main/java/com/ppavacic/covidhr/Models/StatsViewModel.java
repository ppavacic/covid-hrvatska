package com.ppavacic.covidhr.Models;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class StatsViewModel extends ViewModel {

    private final MutableLiveData<Integer> spinnerSelection = new MutableLiveData<>(0);

    public MutableLiveData<Integer> getSpinnerSelection() { return spinnerSelection; }

    public void setSpinnerSelection(int newSpinnerSelection)
        { spinnerSelection.postValue(newSpinnerSelection); }
}