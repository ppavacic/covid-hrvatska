package com.ppavacic.covidhr.Models;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GeoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public GeoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is geo fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}