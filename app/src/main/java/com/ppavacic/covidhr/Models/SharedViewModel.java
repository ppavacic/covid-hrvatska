package com.ppavacic.covidhr.Models;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.ppavacic.covidhr.Common.Notifier;
import com.ppavacic.covidhr.Common.Requester;
import com.ppavacic.covidhr.Common.StrRequest;
import com.ppavacic.covidhr.MainFragments.StatsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.ppavacic.covidhr.R;

public class SharedViewModel extends ViewModel {

    private MutableLiveData<ArrayList<String>> countyNames = new MutableLiveData<>();
    private MutableLiveData<ArrayList<ArrayList<ArrayList<Integer>>>> countyData = new MutableLiveData<>(); //array of counties of array of dates of array of [total, total increase, died, died increase, cured, cured increase, active, active new]
    private MutableLiveData<ArrayList<Date>> countyDates = new MutableLiveData<>();
    
    private MutableLiveData<ArrayList<ArrayList<Integer>>> countryData = new MutableLiveData<>();  //total, total increase, died, died increase, cured, cured increase, active, active new
    private MutableLiveData<ArrayList<Date>> countryDates = new MutableLiveData<>();
    private MutableLiveData<Integer> newsCounty = new MutableLiveData<>(-1);

    public LiveData<ArrayList<String>> getCountyNames() { return countyNames; }
    public LiveData<ArrayList<Date>> getCountyDates() { return countyDates; }
    public LiveData<ArrayList<ArrayList<ArrayList<Integer>>>> getCountyData() { return countyData; }
    public LiveData<ArrayList<Date>> getCountryDates() { return countryDates; }
    public LiveData<ArrayList<ArrayList<Integer>>> getCountryData() { return countryData; }
    public int getUsersCounty(Context context) {
        SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.home_county_ind_key), Context.MODE_MULTI_PROCESS);
        return pref.getInt(context.getString(R.string.home_county_ind_key), 0);
    }
    public LiveData<Integer> getNewsCounty() { return newsCounty; }


    public void setUsersCounty(int newUsersCounty, Context context) {
        SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.home_county_ind_key), Context.MODE_PRIVATE);
        pref.edit().putInt(context.getString(R.string.home_county_ind_key), newUsersCounty).apply();
    };
    public void setNewsCounty(int newNewsCounty) { newsCounty.postValue(newNewsCounty); };


    public void fetchCountryData() {
        String hzzoUrl = "https://www.koronavirus.hr/json/?action=podaci";
        Class[] cArg = new Class[1];
        cArg[0] = JSONArray.class;
        RequestQueue reqQueue = Requester.getInstance().
                getRequestQueue();
        try {
            Method parseCountryDataFun = SharedViewModel.class.getMethod("parseCountryData", cArg);
            StringRequest stringRequest = new StrRequest(hzzoUrl, this, parseCountryDataFun);
            reqQueue.add(stringRequest);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    public void fetchCountyData() {
        String hzzoUrl = "https://www.koronavirus.hr/json/?action=po_danima_zupanijama";
        Class[] cArg = new Class[1];
        cArg[0] = JSONArray.class;
        RequestQueue reqQueue = Requester.getInstance().
                getRequestQueue();
        try {
            Method parseCountryDataFun = SharedViewModel.class.getMethod("parseCountyData", cArg);
            StringRequest stringRequest = new StrRequest(hzzoUrl, this, parseCountryDataFun);
            reqQueue.add(stringRequest);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    //drzava
    @SuppressLint("SimpleDateFormat")
    public void parseCountryData(JSONArray inData) {
        try {
            ArrayList<ArrayList<Integer>> data = new ArrayList<>(inData.length());
            ArrayList<Date> dates = new ArrayList<>(inData.length());

            final String[] dataFields = {"SlucajeviHrvatska", "UmrliHrvatska", "IzlijeceniHrvatska"};

            int[] yesterdaysNumbers = new int[dataFields.length +1]; //for calculating new cases
            for(int i = 0; i < inData.length(); i++) {
                JSONObject oneDay = (JSONObject) inData.get(inData.length() -1 -i);
                String date = oneDay.getString("Datum");
                int j = 0;
                ArrayList<Integer> ondaDayData = new ArrayList<>(dataFields.length *2);
                for(String field : dataFields) {
                    int value = Integer.parseInt(oneDay.getString(dataFields[j]));
                    ondaDayData.add(value);
                    ondaDayData.add(value - yesterdaysNumbers[j]); // for calculating new cases
                    yesterdaysNumbers[j] = value;
                    j++;
                }

                //active cases calucaltion
                //active = tot cases - died - cured
                int active = ondaDayData.get(0) - ondaDayData.get(2) - ondaDayData.get(4);
                ondaDayData.add(active);
                ondaDayData.add(active - yesterdaysNumbers[3]); // new active cases
                yesterdaysNumbers[3] = active;
                //

                data.add(ondaDayData);
                dates.add(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date));
            }
            countryData.postValue(data);
            countryDates.postValue(dates);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
    }

    //zupanije
    @SuppressLint("SimpleDateFormat")
    public void parseCountyData(JSONArray inData) {
        try {
            ArrayList<String> countyNames = new ArrayList<>(21);
            ArrayList<ArrayList<ArrayList<Integer>>> countiesData = new ArrayList<>(21);
            ArrayList<Date> countyDates = new ArrayList<>(inData.length());

            for(int j = 0; j < inData.length(); j++) { //day iteration
                int dayIndex = inData.length() - 1 - j;
                JSONArray countiesDataJ = ((JSONObject) inData.get(dayIndex)).getJSONArray("PodaciDetaljno");
                String countyDate = ((JSONObject) inData.get(dayIndex)).getString("Datum");
                countyDates.add(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(countyDate));

                for(int countyInd = 0; countyInd < 21; countyInd++) { //county iteration
                    ArrayList<Integer> yesterdaysData = new ArrayList<Integer>() {{add(0); add(0); add(0); add(0); add(0); add(0); add(0);}};
                    ArrayList<Integer> countyOneDayData = new ArrayList<>(8);
                    int cases, died, active, cured;
                    JSONObject obj = (JSONObject) countiesDataJ.get(countyInd);

                    if(j == 0) {
                        countyNames.add(obj.getString("Zupanija"));
                        countiesData.add(new ArrayList<>(600));
                    }

                    cases = Integer.parseInt(obj.getString("broj_zarazenih"));
                    died = Integer.parseInt(obj.getString("broj_umrlih"));
                    active = Integer.parseInt(obj.getString("broj_aktivni"));
                    cured = cases - died - active;


                    if(countiesData.get(countyInd).isEmpty()) {
                        yesterdaysData = new ArrayList<Integer>() {{add(0); add(0); add(0); add(0); add(0); add(0); add(0);}};
                    } else {
                        yesterdaysData = countiesData.get(countyInd).get(j-1);
                    }

                    countyOneDayData.add(cases);
                    countyOneDayData.add(cases - yesterdaysData.get(0));
                    countyOneDayData.add(died);
                    countyOneDayData.add(died - yesterdaysData.get(2));
                    countyOneDayData.add(cured);
                    countyOneDayData.add(cured - yesterdaysData.get(4));
                    countyOneDayData.add(active);
                    countyOneDayData.add(active - yesterdaysData.get(6));
                    countiesData.get(countyInd).add(countyOneDayData);
                }
            }
            this.countyNames.postValue(countyNames);
            this.countyDates.postValue(countyDates);
            this.countyData.postValue(countiesData);
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }
    }
}
