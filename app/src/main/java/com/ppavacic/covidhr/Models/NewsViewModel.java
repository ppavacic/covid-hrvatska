package com.ppavacic.covidhr.Models;

import android.util.Log;
import android.util.Pair;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ppavacic.covidhr.Common.ParserAsyncTask;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class NewsViewModel extends ViewModel {
    MutableLiveData<ArrayList<Pair<String, String>>> articles =
            new MutableLiveData<>(new ArrayList<>());

    public LiveData<ArrayList<Pair<String, String>>> getArticles() {
        return articles;
    }

    public void fetchArticles(int county) {
        try {
            int [] linkMap = {  //mapping county from spinners to county news links
                    154, 159, 166, //dubrov
                    140, 165, 146, //karl
                    153, 174, 156,
                    167, 161, 158,
                    155, 162, 144,  //sisacko moslavacka
                    164, 148, 157,
                    163, 160, 143 //zagrebacka
            };
            Class[] cArg = new Class[1];
            cArg[0] = Document.class;
            Method scrapCountyNewsParser = NewsViewModel.class.getMethod("parseArticles", cArg);

            final String hzzoUrl = "https://www.koronavirus.hr/banana/";
            ParserAsyncTask task = new ParserAsyncTask(hzzoUrl + linkMap[county], this, scrapCountyNewsParser);
            task.execute();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void parseArticles(Document doc) {
        Elements perMonth = doc.getElementsByClass("page_content").get(0).select("li"); // articles in format [month, all articles from that month, month, all....]
        int i = 0;
        ArrayList<Pair<String, String>> articles = new ArrayList<>(20);
        for(Element el : perMonth) {
            String title = el.select("strong").text().replaceAll(":", "");
            el.select("strong").remove();
            TextNode tn = el.textNodes().get(0);
            tn.text(tn.text().replaceAll(":", ""));
            String content = el.html();

            articles.add(new Pair<>(title, content));
            if(i++ == 8) break;
        }
        this.articles.postValue(articles);

    }
}