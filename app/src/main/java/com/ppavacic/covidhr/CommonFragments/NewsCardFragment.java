package com.ppavacic.covidhr.CommonFragments;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.ppavacic.covidhr.R;

public class NewsCardFragment extends Fragment {
    private String title, content;
    public NewsCardFragment() { title="undefined"; content="undefined"; };
    public NewsCardFragment(String titleText, String contentText) {
        this.title = titleText;
        this.content = contentText;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_newscard, container, false);

        TextView titleTextView = root.findViewById(R.id.news_title);
        TextView contentTextView = root.findViewById(R.id.news_content);

        titleTextView.setText(title);
        contentTextView.setText(Html.fromHtml(content));
        contentTextView.setMovementMethod(LinkMovementMethod.getInstance());

        return root;
    }
}
