package com.ppavacic.covidhr.CommonFragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.ppavacic.covidhr.Common.Constants;
import com.ppavacic.covidhr.Common.LineChartXAxisValueFormatter;
import com.ppavacic.covidhr.Models.SharedViewModel;
import com.ppavacic.covidhr.Models.StatsViewModel;
import com.ppavacic.covidhr.R;

import java.util.ArrayList;
import java.util.Date;

// TODO: add MPAndroidChart: chart view here, pass data to it
//
public class StatsCardFragment extends Fragment {
    private LineChart chart;
    private final String chartTitle;
    private int displayDataIndex;
    private static int i = 0;
    private ArrayList<ILineDataSet> dataSets = new ArrayList<>(3);
    private SharedViewModel sharedViewModel;
    private int compareDataSetInd = -1;

    public StatsCardFragment() { chartTitle = "No title"; };

    public StatsCardFragment(String chartTitle, int displayDataIndex) {
        this.chartTitle = chartTitle;
        this.displayDataIndex = displayDataIndex; //lame way of passing which data to take from shared viewmodel (new cases, total cases, infected etc)
    }

    //if chart type is BAR im expecting array list of <barentry>
    public void addData(ArrayList<Entry> values, int color, String text) {

        // create a data object with the data sets

        LineDataSet lDataSet = new LineDataSet(values, text);
        //lDataSet.setDrawIcons(false);

        //lDataSet.setFillColor(color);
        lDataSet.setColor(color);
        lDataSet.setCircleColor(color);
        //lDataSet.setFillAlpha(40);

        lDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lDataSet.setCubicIntensity(0.2f);
        lDataSet.setDrawFilled(false);
        lDataSet.setDrawCircles(true);
        lDataSet.setCircleColor(color);
        lDataSet.setDrawCircleHole(true);
        lDataSet.setValueTextColor(getActivity().getTheme().obtainStyledAttributes(new int[]{android.R.attr.textColorSecondary}).getColor(0, Color.RED));
        lDataSet.setValueFormatter(new ValueFormatter() { //value format here, here is the overridden method
            @Override
            public String getFormattedValue(float value) {
                return ""+(int)value;
            }
        });

        dataSets.add(lDataSet);
    }

    public void removeData(int index) {
        if(index >= dataSets.size() || index < 0) return;
        dataSets.remove(index);
    }

    public void updateView() {
        LineData data2 = new LineData(dataSets);
        chart.setData(data2);
        chart.invalidate();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root;
        root = inflater.inflate(R.layout.fragment_stats_card, container, false);

        TextView title = root.findViewById(R.id.cases_chart_title);
        title.setText(chartTitle);
        chart = root.findViewById(R.id.cases_chart_chart);
        chart.getLegend().setEnabled(true);
        chart.getDescription().setEnabled(false);

        XAxis xAxis;
        xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        YAxis yAxis;

        chart.animateX(1000);
        chart.setDrawGridBackground(false);
        yAxis = chart.getAxisLeft();


        yAxis.setDrawGridLines(false);
        chart.getAxisRight().setEnabled(false);
        getResources().getColor(android.R.color.primary_text_dark);
        int textColor = getActivity().getTheme().obtainStyledAttributes(new int[]{android.R.attr.textColorSecondary}).getColor(0, Color.RED);
        chart.getXAxis().setTextColor(textColor);
        chart.getAxisLeft().setTextColor(textColor);
        chart.getLegend().setTextColor(textColor);
        chart.getXAxis().setLabelCount(7, true);
        chart.getXAxis().mAxisMinimum = 100;
        chart.getXAxis().mAxisMaximum = 0;
        chart.setScaleYEnabled(true);
        chart.setAutoScaleMinMaxEnabled(false); //creates epilepsy bug
        chart.getAxisLeft().setStartAtZero(true);

        sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);
        StatsViewModel statsViewModel = new ViewModelProvider(getActivity()).get(StatsViewModel.class);


        int displayNDates = Constants.daysToShow;
        chart.zoom(displayNDates/15f, 1f, displayNDates, 0);
        sharedViewModel.getCountyData().observe(getViewLifecycleOwner(), data -> {

            int countyInd = sharedViewModel.getUsersCounty(getContext());
            addData(countyInd, data);
            // when data is fetched start observing spinner for county with which to compare to
            statsViewModel.getSpinnerSelection().observe(getActivity(), compareCountyInd -> {
                removeData(compareDataSetInd);
                if(compareCountyInd <= 0) { compareDataSetInd = -1; return; };
                compareDataSetInd = addData(compareCountyInd-1, data);
            });
        });

        sharedViewModel.getCountryData().observe(getViewLifecycleOwner(), data -> {
            ArrayList<Entry> entries = new ArrayList<>(600);
            int i = 0;//day
            for(ArrayList<Integer> oneDay : data.subList(data.size()-displayNDates, data.size())) {
                entries.add(new Entry(i++, oneDay.get(displayDataIndex)));
            }
            addData(entries, Color.MAGENTA, "Hrvatska");
            updateView();
            chart.moveViewToX(displayNDates-22);
        });
        return root;
    }

    private synchronized int addData(int countyInd, ArrayList<ArrayList<ArrayList<Integer>>> data) { //return index of added dataset
        if(getContext() == null) return -1;
        String[] countyNames = getContext().getResources().getStringArray(R.array.county_array);
        ArrayList<Entry> entries = new ArrayList<>(600);
        int displayNDates = Constants.daysToShow;
        int i = 0;//day
        for(ArrayList<Integer> oneDay : data.get(countyInd).subList(data.get(0).size()-displayNDates, data.get(0).size())) {
            entries.add(new Entry(i++, oneDay.get(displayDataIndex)));
        }
        addData(entries, Constants.colors[countyInd], countyNames[countyInd]);
        updateView();
        chart.getXAxis().setValueFormatter(new LineChartXAxisValueFormatter(sharedViewModel.getCountryDates().getValue()));
        return dataSets.size()-1;
    }
}
