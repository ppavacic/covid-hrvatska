package com.ppavacic.covidhr.CommonFragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ColorFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.ppavacic.covidhr.Common.Notifier;
import com.ppavacic.covidhr.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class HomeCardFragment extends Fragment {
    private String title = "Nedefinirano";
    private View root = null;
    private LineChart chart;

    private final ValueFormatter formatter = new ValueFormatter() { //value format here, here is the overridden method
        @Override
        public String getFormattedValue(float value) {
            return ""+(int)value;
        }
    };

    public HomeCardFragment() {};
    public HomeCardFragment(String title) { this.title = title; }

    public void updateData(int total, int died, int cured, int active, String title) {
        if(root != null) {
            setTitle(title);
            TextView totalTW, diedTW, curedTW, activeTW;
            totalTW = (TextView) root.findViewById(R.id.cases_value);
            diedTW = root.findViewById(R.id.died_value);
            curedTW = root.findViewById(R.id.cured_value);
            activeTW = root.findViewById(R.id.active_value);

            totalTW.setText(String.valueOf(total));
            diedTW.setText(String.valueOf(died));
            curedTW.setText(String.valueOf(cured));
            activeTW.setText(String.valueOf(active));
        }
    }


    @SuppressLint("SetTextI18n")
    public void setData(int total, int newTotal, int died, int newDied, int cured, int newCured, int active, int newActive) {
        if(root != null) {
            updateData(total, died, cured, active, title);
            TextView totalNTW, diedNTW, curedNTW, activeNTW;
            totalNTW = (TextView) root.findViewById(R.id.cases_new);
            diedNTW = root.findViewById(R.id.died_new);
            curedNTW = root.findViewById(R.id.cured_new);
            activeNTW = root.findViewById(R.id.active_new);

            totalNTW.setText( "+" + newTotal);
            diedNTW.setText("+" + newDied);
            curedNTW.setText("+" + newCured);
            activeNTW.setText(newActive > 0 ? "+ " + String.valueOf(newActive) : String.valueOf(newActive));
        } else {
            Notifier.getInstance().notify(Notifier.CHANNELS.OTHER, "Not loaded", "Fragment updateing while not loaded");
        }
    }

    public synchronized void setDate(String date) {
        if(root != null) {
            TextView dateTW = root.findViewById(R.id.home_card_date);
            dateTW.setText(date);
        }
    }

    public void setTitle(String title) {
        if(root != null) {
            TextView titleTW = root.findViewById(R.id.home_card_title);
            titleTW.setText(title);
        }
    }

    public void setGraphData(ArrayList<Integer> values, boolean animate, int color) {

        ArrayList<Entry> entries = new ArrayList<>(7);

        for(int i = 0; i < values.size() && i < 7; i++) {
            entries.add(new Entry(i+1, values.get(i)));
        }

        LineDataSet set1 = new LineDataSet(entries, "somedata");
        set1.setDrawIcons(false);


        //set1.setFillColor(color);
        set1.setColor(color);
        set1.setFillAlpha(0);

        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set1.setCubicIntensity(0.2f);
        set1.setDrawFilled(true);
        set1.setDrawCircles(true);
        set1.setCircleColor(color);
        set1.setValueTextSize(20);
        set1.setValueTextColor(
                getActivity().getTheme().obtainStyledAttributes(
                        new int[]{android.R.attr.textColorSecondary}).getColor(0, Color.RED));

        set1.setValueFormatter(formatter);
        // line thickness and point size
        set1.setLineWidth(0);

        // draw points as solid circles
        set1.setDrawCircleHole(false);

        LineData data = new LineData(set1);
        chart.setData(data);
        if(animate) chart.animateY(300);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home_card, container, false);
        TextView titleTW = root.findViewById(R.id.home_card_title);
        chart = root.findViewById(R.id.home_card_chart);
        titleTW.setText(title);

        chart.setTouchEnabled(false);
        chart.getDescription().setText("Novozaraženih 7 dana");
        chart.getDescription().setTextColor(getActivity().getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.textColorHint}).getColor(0, Color.RED));
        //chart.getDescription().setEnabled(false);
        chart.getDescription().setTextSize(15);
        chart.getXAxis().setDrawGridLines(false);
        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().setDrawLabels(false);
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);

        ArrayList<Integer> values = new ArrayList<>(1);
        values.add(0);
        setGraphData(values, false, Color.parseColor("#A9A9A9"));
        return root;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
