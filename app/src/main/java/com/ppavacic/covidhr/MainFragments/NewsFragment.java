package com.ppavacic.covidhr.MainFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.ppavacic.covidhr.CommonFragments.NewsCardFragment;
import com.ppavacic.covidhr.Models.SharedViewModel;
import com.ppavacic.covidhr.R;
import com.ppavacic.covidhr.Models.NewsViewModel;

import java.util.ArrayList;


public class NewsFragment extends Fragment {

    private NewsViewModel newsViewModel;
    private SharedViewModel sharedViewModel;
    private static int lastCountyInd = -1;
    private Spinner spinner;

    @Override
    public void onStart() {
        super.onStart();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        newsViewModel =
                new ViewModelProvider(getActivity()).get(NewsViewModel.class);
        sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);
        View root = inflater.inflate(R.layout.fragment_news, container, false);
        spinner = (Spinner) root.findViewById(R.id.news_county_spinner);
        //SharedPreferences pref = getContext().getSharedPreferences(getString(R.string.home_county_ind_key), Context.MODE_PRIVATE);
        int initSpinnerSelection = sharedViewModel.getNewsCounty().getValue();
        if(sharedViewModel.getNewsCounty().getValue() == -1) { //read intent county only on first run
            initSpinnerSelection = getActivity().getIntent().getIntExtra("news_county", sharedViewModel.getUsersCounty(getActivity()));
        }
        sharedViewModel.setNewsCounty(initSpinnerSelection);
        spinner.setSelection(initSpinnerSelection);

        //if data isnt already fetched fetch it.
        if(lastCountyInd != initSpinnerSelection) { newsViewModel.fetchArticles(initSpinnerSelection); lastCountyInd = initSpinnerSelection; }
        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newsViewModel.getArticles().observe(getActivity(), this::displayArticles);
        displayArticles(newsViewModel.getArticles().getValue());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { updateSpinner(position); }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void displayArticles(ArrayList<Pair<String, String>> articleData) {
        if(!isAdded()) return;
        FragmentTransaction activeTrans = getChildFragmentManager().beginTransaction();
        activeTrans.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        int i = 0;
        for(Pair<String, String> article : articleData) {
            NewsCardFragment card = new NewsCardFragment(article.first, article.second);
            if(i++ == 0) activeTrans.replace(R.id.fragment_news, card, article.first);
            else activeTrans.add(R.id.fragment_news, card, article.first);
        }
        activeTrans.commit();
    }


    void updateSpinner(int position) {
        if(lastCountyInd != position || newsViewModel.getArticles().getValue().isEmpty()) {
            sharedViewModel.setNewsCounty(spinner.getSelectedItemPosition());
            newsViewModel.fetchArticles(position); lastCountyInd = position;
        }
    }
}