package com.ppavacic.covidhr.MainFragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.RequestQueue;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.ppavacic.covidhr.CommonFragments.StatsCardFragment;
import com.ppavacic.covidhr.Models.SharedViewModel;
import com.ppavacic.covidhr.Models.StatsViewModel;
import com.ppavacic.covidhr.R;
import com.ppavacic.covidhr.Common.Requester;

import java.security.Provider;
import java.util.ArrayList;


public class StatsFragment extends Fragment {

    private ArrayList<ArrayList<Entry>> cumulativeData;
    private ArrayList<BarEntry> newCasesData;
    private StatsCardFragment activeCard, newCasesCard, diedCard;
    private StatsViewModel statsViewModel;
    private Spinner spin;

    private final String[] dataFields = {
            "SlucajeviHrvatska", "UmrliHrvatska", "IzlijeceniHrvatska"}; //+ new cases


    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        newCasesCard = new StatsCardFragment("Novozaraženi", 1);
        diedCard = new StatsCardFragment("Umrli", 3);
        activeCard = new StatsCardFragment("Aktivnih", 6);
        RequestQueue reqQueue = Requester.getInstance(getActivity().getApplicationContext()).
                getRequestQueue();
        statsViewModel = new ViewModelProvider(getActivity()).get(StatsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_stats, container, false);
        spin = root.findViewById(R.id.stats_county_spinner);


        FragmentTransaction activeTrans = getChildFragmentManager().beginTransaction();

        activeTrans.replace(R.id.stats_placeholder_1, newCasesCard);
        activeTrans.replace(R.id.stats_placeholder_2, activeCard);
        activeTrans.replace(R.id.stats_placeholder_3, diedCard);
        activeTrans.commit();
        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spin.setSelection(statsViewModel.getSpinnerSelection().getValue());
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { updateSpinner(position); }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    public void updateSpinner(int newPosition) {
        statsViewModel.setSpinnerSelection(newPosition);
    }
}