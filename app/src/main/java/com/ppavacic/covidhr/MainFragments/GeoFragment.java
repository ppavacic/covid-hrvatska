package com.ppavacic.covidhr.MainFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ppavacic.covidhr.Common.LocationItem;
import com.ppavacic.covidhr.R;
import com.ppavacic.covidhr.Models.GeoViewModel;

import java.util.Locale;

public class GeoFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnInfoWindowClickListener {

    private Context context;
    private static boolean infoShown = false;
    private FusedLocationProviderClient fusedLocationClient;
    private final static String []covidTestingTxt = {"Mjesto za testiranje na SARS-CoV-2", "Mjesto masovnog cijepljenja"};
    //source: https://www.koronavirus.hr/svi-testni-centri-u-republici-hrvatskoj/763
    private final LocationItem[] testingSites = {
        //PGŽ
        new LocationItem(45.33228711001263, 14.42569728283081, "NZZJZ PRIMORSKO-GORANSKE ŽUPANIJE", "051 334 530", "mis.kancelarija@zzjzpgz.hr", covidTestingTxt[0],
                "Nakon radnog vremena:\n" +
                "091 1435163,\n" +
                "091 7234573\n" +
                "mis.kancelarija@zzjzpgz.hr\n" +
                "Pravne osobe:  narucivanje.poslovni@zzjzpgz.hr"),
        new LocationItem(44.5347193283088, 14.467297577970726, "Dom zdravlja Mali Lošinj", "051 233 574", "senka.stojanovic@zzjzpgz.hr", covidTestingTxt[0]),
        new LocationItem(44.962229426092996, 14.408550263396808, "Dom zdravlja Cres", "051 572 218", "epid.cres@zzjzpgz.hr", covidTestingTxt[0]),
        new LocationItem(44.76391513055368, 14.75779595753963, "Dom zdravlja Rab", "051 776 924", "epidemiologija.rab@zzjzpgz.hr", covidTestingTxt[0]),
        new LocationItem(45.02862109320215, 14.569826936956572, "Dom zdravlja Krk", "051 221 955", "epidemiologija.krk@zzjzpgz.hr", covidTestingTxt[0]),
        new LocationItem(45.17570349357791, 14.694630816836671, "Dom zdravlja Crikvenica", "051 241 055", "", covidTestingTxt[0]),
        new LocationItem(45.33579190764311, 14.306000855356624, "Dom zdravlja Opatija", "051 718 067", "epidemiologija.opatija@zzjzpgz.hr", covidTestingTxt[0], "Lokacija testiranja: Ulica Ive Kaline, kod objekta iznad hotela Belvedere"),
        new LocationItem(45.39808104318577, 14.803950199849744, "Dom zdravlja Delnice", "051 814 097", "epidemiologija.delnice@zzjzpgz.hr", covidTestingTxt[0]),
        new LocationItem(45.33104764241088, 14.428505501003853, "KBC Rijeka " +
                "Klinički zavod za kliničku mikrobiologiju", "051 658 844", "pcr-covid@kbc-rijeka.hr", covidTestingTxt[0]),
        //Istra
        new LocationItem(44.86153584916105, 13.849328334326684, "Zavod za javno zdravstvo Istarske županije", "099 5298 222", "covidtest@zzjziz.hr", covidTestingTxt[0]),
        new LocationItem(44.86781651712028, 13.852483500079623, "Dom zdravlja", "095 4321 879", "", covidTestingTxt[0]),
        new LocationItem(45.10012370318456, 14.118540016462124, "Dom zdravlja", "095 4321 881", "", covidTestingTxt[0]),
        //new LocationItem(0, 0, "Dom zdravlja", "095 4321 874", "X", covidTestingTxt[0]), //TODO labin ulica bez broja
        new LocationItem(45.23898974669158, 13.94051907133335, "Dom zdravlja", "095 4321 875", "", covidTestingTxt[0]),
        new LocationItem(45.22733342851135, 13.605613035329212, "Dom zdravlja", "095 4321 876 ", "", covidTestingTxt[0]),
        new LocationItem(45.40877398073442, 13.963405437453863, "Dom zdravlja", "095 4321 882", "", covidTestingTxt[0]),
        new LocationItem(45.4327864179499, 13.524641581615617, "Dom zdravlja", "095 4321 880", "", covidTestingTxt[0]),
        //Ličko-senjska
        new LocationItem(44.547722885303784, 15.375021949782333, "Zavod za javno zdravstvo Ličko-senjske županije", "053/573-656", "mikrobiologija@zzjzlsz.hr", covidTestingTxt[0]),
        //Međimurska
        new LocationItem(46.393092782061736, 16.4344337346916, "Zavod za javno zdravstvo Međimurske županije, COVID kontejneru na parkiralištu iza", "040 386 841", "mikrobiologija@zzjz-ck.hr", covidTestingTxt[0]),
        //Osječko baranjska
        new LocationItem(45.558307379654984, 18.68767333993607, "Zavod za javno zdravstvo", "031 225 771", "mikrobiologija.zzjz@gmail.com", covidTestingTxt[0]),
        new LocationItem(45.55790356151497, 18.71371111238431, "KBC Osijek, testiranje u dvorištu Zavoda za dermatologiju i venerologiju; adresa: Park kraljice Katarine Kosača 10", "031 511 050", "", covidTestingTxt[0]),
        //pozesko slavonska
        new LocationItem(45.34410087068217, 17.69932949603275, "OŽB Požega", "034 254 548", "testiranje.covid19@pozeska-bolnica.hr", covidTestingTxt[0], "Obavezna najava nekoliko dana ranije"),
        new LocationItem(45.33238226419764, 17.67586596538414, "Zavod za javno zdravstvo", "", "", covidTestingTxt[0],
                "Uzorci se uzimaju:\n" +
            "Dom zdravlja PSŽ, Matije Gupca 10 (pon., sri., pet.)\n" +
            "Pakrac, Bolnička 68 (uto. i čet.; kod Doma zdravlja)"),
        //Grad Zagreb
        new LocationItem(45.827661294943866, 15.981601271559137, "HRVATSKI ZAVOD ZA JAVNO ZDRAVSTVO", "", "", covidTestingTxt[0],
                "Naručivanje je obavezno\n" +
                "Po narudžbi\n" +
                "na covid@hzjz.hr (za osobe s uputnicom)\n" +
                "ili online\n" +
                "http://covid.hzjz.hr/termin/testiranje-na-korona-virus/ (za osobe bez uputnice)\n" +
                "\n" +
                "Brzi antigenski test (vrijeme čekanja nalaza 24 sata) \n" +
                "Naručivanje na covid@hzjz.hr"),
        new LocationItem(45.83141635122341, 15.980551508372566, "NZJZ DR. ANDRIJA ŠTAMPAR", "", "", covidTestingTxt[0],
                "Poslovni korisnici: covidtest@stampar.hr\n" +
                "Privatni korisnici putem online naručivanja:\n" +
                "https://prijavnicentar.hr/testiranje/stampar\n" +
                "01 46 96 308, 01 55 63 102, 01 46 96 315"),
        new LocationItem(45.82976730351208, 15.98017457745472, "KLINIKA ZA INFEKTIVNE BOLESTI \"DR. FRAN MIHALJEVIĆ\"", "", "", covidTestingTxt[0],
                "Obavezno naručivanje za testiranja na zahtjev na https://narucivanje.bfm.hr ; \n" +
                "S uputnicom nema naručivanja\n" +
                "Ostale informacije na\n" +
                "https://bfm.hr/testiranje-na-sars-cov-2/"),
        new LocationItem(45.77279660272795, 15.979693297175974, "DOM ZDRAVLJA ZAGREB - CENTAR", "01 6598 464", "", covidTestingTxt[0]),
        new LocationItem(45.83041100252116, 16.052318729927453, "DOM ZDRAVLJA ZAGREB - ISTOK", "091 2350 182", "", covidTestingTxt[0]),
        new LocationItem(45.811414409165735, 15.94428542530475, "DOM ZDRAVLJA ZAGREB - ZAPAD", "091 3876 591", "", covidTestingTxt[0]),
        new LocationItem(45.797418492282816, 15.949494706235146, "DOM ZDRAVLJA ZAGREB - ZAPAD", "091 3876 000", "", covidTestingTxt[0]),
        new LocationItem(45.78026676050522, 15.97887904807928, "Istočno parkiralište Zagrebačkog Velesajma", "", "", covidTestingTxt[0], "Isključivo uz prethodnu narudžbu putem http://brzitest.com.hr/ "),

        //sisacko moslavacka
        new LocationItem(45.48387071749093, 16.37484898171028, "", "", "", covidTestingTxt[0],
                "Testiranje se provodi u kontejneru kraj bazena\n" +
                "Testiranje u Petrinji provodi se u kontejneru pokraj laboratorija (Lopašićeva ulica)"),
        //varaždinska
        new LocationItem(46.22412016340955, 16.126972556366653, "ZJZ VARAŽDINSKE ŽUPANIJE", "042 653 130", "epidemiologija@zzjzzv.hr", covidTestingTxt[0], "Radnim danom od 10:00 - 14:00 sati"),
        //new LocationItem(0, 0, "", "", "", covidTestingTxt[0]), //Lokacija upitna. nova lokacija kod arene varaždin?

        //virovitičko podravska
        new LocationItem(45.8285819004839, 17.39022659516343, "Zavod za javno zdravstvo \"Sveti Rok\"", "+385995826909", "", covidTestingTxt[0],
                "Isključivo po prethodnoj najavi i dogovoru na broj telefona +385995826909 i +385995826920 (ponedjeljak-petak od 13 do 15 sati, subota-nedjelja od 9 do 12 sati)\n" +
                        "Uz ispunjeni obrazac za testiranje\n" +
                        "Uzimanje uzoraka provodi se  i na lokaciji: Virovitica, Ljudevita Gaja 21 (Izdvojen kontejner - ulaz kod željezničkog kolodvora – male stanice) od 7:30 do 8:30 sati."),
        //vukovarsko srijemska
        new LocationItem(45.29210210239524, 18.817748300994324, "ZJZ Vukovarsko-srijemske županije", "032/ 349-251", "", covidTestingTxt[0],
                "Ulaz preko puta autobusne stanice\n" +
                "Obavezno je naručivanje, koje se vrši svakim radnim danom u vremenu od 11:00 – 14:00 sati na brojeve: 032/ 349-251 i 091 270 0360"),
        //zadarska
        new LocationItem(44.10808760972597, 15.235440536574835, "", "023 643 380", "", covidTestingTxt[0],
                "023 643 380 radnim danom od 11:00 - 12:00\n" +
                "ili online\n" +
                "https://outlook.office365.com/owa/calendar/\n" +
                "Zavodzajavnozdravstvo\n" +
                "Zadar1@zadarzavod.onmicrosoft.com/bookings/"),
        new LocationItem(44.10736288078501, 15.234579395054663, "Opće bolnica Zadar", "023/505 008", "covid19.test@bolnica-zadar.hr ", covidTestingTxt[0]),

        //Zagrebačka županija
        new LocationItem(45.857641492882266, 15.802587075378476, "DOM ZDRAVLJA ZAGREBAČKE ŽUPANIJE", "01 3315 906", "", covidTestingTxt[0]),
        //new LocationItem(45.70760907442848, 16.38770074552002, "", "", "", covidTestingTxt[0]), //ZJZ ZAGREBAČKE ŽUPANIJE, Dugo Selo, Đure Dubenika 2
        new LocationItem(45.71381435278593, 16.06590124494368, "DOM ZDRAVLJA ZAGREBAČKE ŽUPANIJE", "01 6379 717", "", covidTestingTxt[0],
                "Od 19.4. testiranje će se provoditi na prostoru VIP parkirališta Gradskog stadiona od ponedjeljka do petka u periodu od 8 do 10:30 sati."),
        new LocationItem(45.80545770244141, 15.713038367724632, "DOM ZDRAVLJA ZAGREBAČKE ŽUPANIJE", "", "", covidTestingTxt[0]),
        new LocationItem(45.798901181653974, 15.815003884886753, "ZJZ ZAGREBAČKE ŽUPANIJE", "099/582-9979", "", covidTestingTxt[0], "01/3323253, 099/582-9979 ili preko puta online platforme"),
        //Dubrov
        new LocationItem(42.647738961265894, 18.075866509195382, "OB DUBROVNIK", "", "info@covid.test@bolnica-du.hr ", covidTestingTxt[0],
                "Pacijenti upućeni od liječnika obiteljske medicine bez naručivanja; komercijalna testiranja se naručuju na e-mail info@covid.test@bolnica-du.hr "),
        new LocationItem(42.647977469872004, 18.07645790754729, "ZJZ DUBROVAČKO-NERETVANSKE ŽUPANIJE", "", "mikrobiologija@zzjzdnz.hr", covidTestingTxt[0]),

        new LocationItem(42.64746921146273, 18.094139495474078, "DOM ZDRAVLJA DUBROVNIK", "020 641 613", "covid19@dom-zdravlja-dubrovnik.hr", covidTestingTxt[0],
                "Naručivanje putem nadležnog liječnika ili telefonski\n" +
                        "020 641 613,\n" +
                        "099 5291 888"),
        new LocationItem(42.66176909928658, 18.071094010501717, "Dom zdravlja Dubrovnik", "020 641 613", "covid19@dom-zdravlja-dubrovnik.hr", covidTestingTxt[0]),
        new LocationItem(42.94968550315714, 17.137288271341877, "DOM ZDRAVLJA KORČULA - KORČULA", "099 5298 734", "amb.za.covid@dom-zdravlja-korcula.hr", covidTestingTxt[0]),
        new LocationItem(42.97716195538435, 17.177785864432327, "DOM ZDRAVLJA KORČULA – OREBIĆ", "099 5298 734", "amb.za.covid@dom-zdravlja-korcula.hr", covidTestingTxt[0]), //TODO: DOM ZDRAVLJA KORČULA – OREBIĆ

        //splitsko dalmatinska
        new LocationItem(43.50378448138348, 16.457835304506517, "KBC SPLIT", "", "", covidTestingTxt[0], "Nema mogućnosti naručivanja."),
        new LocationItem(43.51113005658302, 16.44994668390568, "NZJZ SPLITSKO-DALMATINSKE ŽUPANIJE", "", "", covidTestingTxt[0], "Naručivanje isključivo putem nadležnog liječnika ili online https://prijavnicentar.hr/hr_HR/testiranje/split"),
        new LocationItem(43.292353608703145, 17.022101996118458, "Dom zdravlja Makarska", "", "", covidTestingTxt[0], "Istočno parkiralište Doma zdravlja Makarska\t"),
        new LocationItem(43.70445378596894, 16.652976138016594, "NZJZ SDŽ Sinj Gradski stadion NK Junak", "", "", covidTestingTxt[0],
                "Isključivo uz prethodno naručivanje (od 30.11.)\n" +
                        "https://www.nzjz-split.hr/index.php/2-uncategorised/455-testiranje-na-sars-cov-2-online-narucivanje"),
        //šibensko kninska
        new LocationItem(43.73313007145672, 15.901509175540685, "ZJZ ŠIBENSKO-KNINSKE ŽUPANIJE", "022 341 232", "narudzba.covid@zzjz-sibenik.hr", covidTestingTxt[0]),
        new LocationItem(43.73200723466234, 15.89923525893015, "OB ŠIBENIK", "", "", covidTestingTxt[0]),
        new LocationItem(44.052465854178216, 16.214784172032235, "OPĆA VETERANSKA BOLNICA \"HRVATSKI PONOS\"", "", "", covidTestingTxt[0]),
        //new LocationItem(0, 0, "Punkt za testiranje turista", "", "", covidTestingTxt[0]),

        //brodsko-posavska
        new LocationItem(45.162911409557786, 18.008549194135398, "Izdvojena ambulanta", "035 411 004", "", covidTestingTxt[0]),
        new LocationItem(45.15625313067303, 18.025880940703928, "ZJZ Brodsko-posavske županije", "035/444-769", "", covidTestingTxt[0],
                "Obavezna najava\n" +
                "Radnim danom od 10:30 do 13:30 h"),

        new LocationItem(45.150404696799846, 18.0215983786691, "Dom zdravlja S. Brod Drive-in", "035/411-004", "", covidTestingTxt[0], "Samo na uputnicu uz obveznu prethodnu narudžbu"),

        //new LocationItem(0, 0, "", "", "", covidTestingTxt[0]),
    };

    private final LocationItem[] vaccinationSites = {
            new LocationItem(45.344597569718324, 14.384094719630411, "Dvorana Zamet", "051 666 671", "", covidTestingTxt[1]),
            new LocationItem(45.34461116802844, 14.380938897412689, "Makrocentar Zamet", "091 251 8544", "", covidTestingTxt[1]),
            new LocationItem(45.34480223881042, 14.314082068465273, "Sportska dvorana Marino Cvetković", "051401362", "", covidTestingTxt[1]),
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        GeoViewModel geoViewModel = new ViewModelProvider(this).get(GeoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_geo, container, false);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        context = getContext();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) { //if permissions arent
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions

        } else {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            LatLng usersLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(usersLocation));
                            googleMap.addMarker(new MarkerOptions()
                                    .position(usersLocation)
                                    .icon(BitmapDescriptorFactory.defaultMarker(190))
                                    .title("Vaša lokacija")
                                    .snippet("Klik na druge lokacije za više mogućnosti")).setTag("my_location");
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(usersLocation, 9.0f));
                        }
                    });
        }
        int i = 0;
        for (LocationItem testingSite : testingSites) {
            googleMap.addMarker(testingSite.getMarker(90)).setTag(i++);
        }

        for(LocationItem vaccSite : vaccinationSites) {
            googleMap.addMarker(vaccSite.getMarker(40)).setTag(i++);
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(44.84212370773419, 16.31369633036516), 6.75f));
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);

    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        if(marker.getTag().getClass() == String.class && ((String) marker.getTag()).compareTo("my_location") == 0) return false;

        if(!infoShown) {
            Toast.makeText(getContext(), "Klik na info prozor za više mogućnosti!", Toast.LENGTH_SHORT).show();
            infoShown = true;
        }
        return false;
    }

    @Override
    public void onMapLongClick(@NonNull LatLng latLng) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=12=q=%f,%f", latLng.latitude, latLng.longitude, latLng.latitude, latLng.longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }

    @Override
    public void onInfoWindowClick(@NonNull Marker marker) {
        if(marker.getTag().getClass() == String.class && ((String) marker.getTag()).compareTo("my_location") == 0) return;

        int itemIndex = (int) marker.getTag();
        LocationItem locItem;

        //stupid way, but fastest. avoiding use of hash map for locations
        if(itemIndex < testingSites.length) locItem = testingSites[itemIndex];
        else locItem = vaccinationSites[itemIndex-testingSites.length];

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(locItem.getTitle());

        builder.setMessage(locItem.getDescription());
        builder.setPositiveButton("Otvori navigaciju", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                String uri = String.format(Locale.ENGLISH, "google.navigation:q=%f,%f", locItem.getLat() ,locItem.getLon());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                context.startActivity(intent);
            }
        });
        if(locItem.getPhoneNumber().compareTo("") != 0) {
            builder.setNeutralButton("Nazovi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    String uri = String.format(Locale.ENGLISH, "tel:%s", locItem.getPhoneNumber());
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
                    context.startActivity(intent);
                }
            });
        }
        if(locItem.getEmail().compareTo("") != 0) {
            builder.setNegativeButton("Pošalji e-mail", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("plain/text");
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[] { locItem.getEmail() });
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Upit vezan za Covid-19 testiranje");
                    intent.putExtra(Intent.EXTRA_TEXT,
                            "Poštovani, \n\nkontaktiram vas vezano za covid testiranje u vašem objektu " + locItem.getTitle() + "\n" +
                                    "Zanima me ... " +
                                    "\n\nLijep pozdrav!\n\n" +
                                    "Poslano koristeći Crovid aplikaciju.");
                    startActivity(Intent.createChooser(intent, ""));
                }
            });
        }
        builder.show();
    }
}