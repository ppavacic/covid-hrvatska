package com.ppavacic.covidhr.MainFragments;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.ppavacic.covidhr.Common.Constants;
import com.ppavacic.covidhr.CommonFragments.HomeCardFragment;
import com.ppavacic.covidhr.Models.SharedViewModel;
import com.ppavacic.covidhr.R;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private ArrayList<String> countyNames = new ArrayList<>(21);;
    private ArrayList<Integer[]> countyCases = new ArrayList<>(21);;
    private String countyLastDate = "Nepoznat datum";
    private HomeCardFragment countyCasesCard = null, countryCasesCard = null;
    private SharedViewModel sharedViewModel;
    Spinner spinner;

    @Override
    public void onStart () {
        super.onStart();
        sharedViewModel = new ViewModelProvider(getActivity()).get(SharedViewModel.class);

        int spinSel = sharedViewModel.getUsersCounty(getActivity());
        spinner.setSelection(spinSel);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateCountyCard(sharedViewModel.getCountyData().getValue());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //country
        sharedViewModel.getCountryData().observe(getActivity(), countryData -> {
            int lastDayInd = countryData.size() -1;
            ArrayList<Integer> lastDay = countryData.get(lastDayInd);
            countryCasesCard.setData(
                    lastDay.get(0), lastDay.get(1), lastDay.get(2), lastDay.get(3),
                    lastDay.get(4), lastDay.get(5), lastDay.get(6), lastDay.get(7));

            ArrayList<Integer> values = new ArrayList<>(7);
            for(int i = 0; i < 7; i++) {
                values.add(countryData.get(lastDayInd-6 +i).get(1));
            }
            countryCasesCard.setGraphData(values, true, Color.MAGENTA);

        });
        sharedViewModel.getCountryDates().observe(getActivity(), countryDates -> {
            countryCasesCard.setDate(countryDates.get(countryDates.size() -1).toLocaleString());
        });

        //zupanije
        sharedViewModel.getCountyData().observe(getActivity(), this::updateCountyCard);

        sharedViewModel.getCountyDates().observe(getActivity(), countyDates -> {
            countyCasesCard.setDate(countyDates.get(countyDates.size() -1).toLocaleString());
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        FragmentTransaction activeTrans = getChildFragmentManager().beginTransaction();
        countryCasesCard = new HomeCardFragment("Hrvatska");
        countyCasesCard = new HomeCardFragment("Županija");
        activeTrans.replace(R.id.frame_card_cases, countryCasesCard);
        activeTrans.replace(R.id.frame_card_county_cases, countyCasesCard);
        activeTrans.commit();

        //mught have to move to onviewcreated TODO
        spinner = (Spinner) root.findViewById(R.id.home_county_spinner);
        return root;
    }

    void updateCountyCard(ArrayList<ArrayList<ArrayList<Integer>>> countiesData) {
        if(countiesData == null /*|| spinner.getSelectedItemPosition() == sharedViewModel.getUsersCounty(getActivity())*/) return;
        //SharedPreferences pref = getContext().getSharedPreferences(getString(R.string.home_county_ind_key), Context.MODE_PRIVATE);
        //pref.edit().putInt(getString(R.string.home_county_ind_key), spinner.getSelectedItemPosition()).apply();
        int selectedCounty = spinner.getSelectedItemPosition();
        sharedViewModel.setUsersCounty(selectedCounty, getContext());

        ArrayList<ArrayList<Integer>> usersCountyData = countiesData.get(selectedCounty);
        int lastDayInd = countiesData.get(0).size() -1;
        ArrayList<Integer> lastDay = usersCountyData.get(lastDayInd);
        countyCasesCard.setData(lastDay.get(0), lastDay.get(1), lastDay.get(2), lastDay.get(3),
                lastDay.get(4), lastDay.get(5), lastDay.get(6), lastDay.get(7));
        countyCasesCard.setTitle(sharedViewModel.getCountyNames().getValue().get(selectedCounty));

        ArrayList<Integer> values = new ArrayList<>(7);
        for(int i = 0; i < 7; i++) {
            values.add(usersCountyData.get(lastDayInd-6 +i).get(1));
        }
        countyCasesCard.setGraphData(values, true, Constants.colors[selectedCounty]);

    }
}