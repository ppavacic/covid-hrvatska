package com.ppavacic.covidhr;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.internal.location.zzaf;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ppavacic.covidhr.Common.Notifier;
import com.ppavacic.covidhr.Common.Requester;
import com.ppavacic.covidhr.GeoFencing.GeoFencingClient;
import com.ppavacic.covidhr.Models.SharedViewModel;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static GeoFencingClient geoClient;
    private static final int LOCATION_REQ_CODE = 567;
    //private static boolean initialized = false;

    public MainActivity() {}

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Notifier.getInstance(this); //initialization dont delete this
        Requester.getInstance(this); //initialization dont delete this
        SharedViewModel sharedData = new ViewModelProvider(this).get(SharedViewModel.class);

        int intentCounty = getIntent().getIntExtra("news_county", sharedData.getUsersCounty(this));
        if(intentCounty != sharedData.getUsersCounty(this)) {
            sharedData.setUsersCounty(intentCounty, this);
            Toast.makeText(this, "Županija promijenjena!", Toast.LENGTH_SHORT).show();
        }

        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_stats, R.id.navigation_news, R.id.navigation_geo)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        //if(!initialized) {
            int initalFragment = getIntent().getIntExtra("navigate_id_fragment", R.id.navigation_home); // if app is being opened by notifier
            navController.navigate(initalFragment);
            //initialized = true;
        //}

        sharedData.fetchCountyData();
        sharedData.fetchCountryData();

        //request location permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            boolean shouldRequest = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
            /*if(shouldRequest) */ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION,}, LOCATION_REQ_CODE); //, Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
            if(!shouldRequest) Toast.makeText(this, "Lokacijske usluge odbijene!", Toast.LENGTH_SHORT).show();
            else Toast.makeText(this, "Lokacijske usluge ukljucene!", Toast.LENGTH_SHORT).show();
        } else {
            if(geoClient == null) geoClient = new GeoFencingClient(this);  //UNCOMMENT THIS
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_REQ_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    geoClient = new GeoFencingClient(this);
                    Toast.makeText(this, "Lokacijske usluge prihvacene!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Pristup lokaciji odbijen, lokacijsko obavještavanje onemogućeno!", Toast.LENGTH_LONG).show();

                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}