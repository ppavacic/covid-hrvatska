package com.ppavacic.covidhr.Common;

import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.time.temporal.ChronoUnit.*;

import static java.time.temporal.ChronoUnit.DAYS;

public class Constants {
    public static final int []colors = {
            Color.parseColor("#5ef28b"),
            Color.parseColor("#e6f25e"),
            Color.parseColor("#ff0000"),
            Color.parseColor("#9500ff"),
            Color.parseColor("#1e00ff"),
            Color.parseColor("#00bbff"),
            Color.parseColor("#00f7ff"),
            Color.parseColor("#00ff95"),
            Color.parseColor("#00ff33"),
            Color.parseColor("#99ff00"),
            Color.parseColor("#eeff00"),
            Color.parseColor("#ff9100"),
            Color.parseColor("#ff5500"),
            Color.parseColor("#ff008c"),
            Color.parseColor("#7b00ff"),
            Color.parseColor("#2b00ff"),
            Color.parseColor("#00bbff"),
            Color.parseColor("#00ffae"),
            Color.parseColor("#00ff3c"),
            Color.parseColor("#99ff00"), //20
            Color.MAGENTA,
            Color.CYAN,
    };

    /*private static final String startDateStr = "March 21, 2020";
    private static final DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());*/
    public static final int daysToShow = 400;

}
