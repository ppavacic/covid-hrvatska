package com.ppavacic.covidhr.Common;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class Requester {
    private static Requester instance;
    private RequestQueue requester;
    private static Context ctx;

    private Requester(Context context) {
        ctx = context;
        requester = getRequestQueue();
    }

    public static synchronized Requester getInstance(Context context) {
        if (instance == null) {
            instance = new Requester(context);
        }
        return instance;
    }

    public static synchronized Requester getInstance() {
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requester == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requester = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requester;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}