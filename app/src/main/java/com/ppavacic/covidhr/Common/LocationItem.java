package com.ppavacic.covidhr.Common;

import android.content.res.Resources;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationItem {
    private LatLng location;
    private final String title, phoneNumber, email, institutionType, snippetString;
    public LocationItem(double lat, double lon, String title, String phoneNumber, String email, String institutionType) {
        this.location = new LatLng(lat, lon);
        this.title = title;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.snippetString = "";
        this.institutionType = institutionType;
    }
    public LocationItem(double lat, double lon, String title, String phoneNumber, String email, String institutionType, String snippet) {
        this.location = new LatLng(lat, lon);
        this.title = title;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.institutionType = institutionType;
        this.snippetString = snippet;
    }

    public MarkerOptions getMarker(int hue) {
        return new MarkerOptions()
                .title(title)
                .icon(BitmapDescriptorFactory.defaultMarker(hue))
                .snippet(institutionType)
                .position(location);
    }

    public String getDescription() {
        if(snippetString.compareTo("") == 0) {
            return institutionType;
        }
        return snippetString;
    }

    public String getTitle() { return title; }
    public String getSnippet() { return snippetString; }
    public String getPhoneNumber() { return phoneNumber; }
    public String getEmail() { return email; }
    public LatLng getLocation() { return location; }
    public double getLat() { return location.latitude; }
    public double getLon() { return location.longitude; }
}
