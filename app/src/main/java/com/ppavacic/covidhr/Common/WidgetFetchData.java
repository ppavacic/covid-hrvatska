package com.ppavacic.covidhr.Common;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Pair;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.ppavacic.covidhr.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class WidgetFetchData extends AsyncTask<Void, Void, String> {
    private final Context context;
    private final AppWidgetManager appWidgetManager;
    private final int[] appWidgetIds;
    private ArrayList<Pair<String, String>> articles;
    private final String url = "https://www.koronavirus.hr/banana/";
    private int county;


    public WidgetFetchData(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds, ArrayList<Pair<String, String>> articles, int county) { // url for request, object, and objects method on which to execute
        this.context = context;
        this.appWidgetManager = appWidgetManager;
        this.appWidgetIds = appWidgetIds;
        this.articles = articles;
        this.county = county;
    }
    @Override
    protected String doInBackground(Void... voids) {
        try {
            int [] linkMap = {  //mapping county from spinners to county news links
                    154, 159, 166, //dubrov
                    140, 165, 146, //karl
                    153, 174, 156,
                    167, 161, 158,
                    155, 162, 144,  //sisacko moslavacka
                    164, 148, 157,
                    163, 160, 143 //zagrebacka
            };

            Document doc = Jsoup.connect(url + String.valueOf(linkMap[county])).get();
            parseArticles(doc, context, appWidgetManager, appWidgetIds);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void parseArticles(Document doc, Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Elements perMonth = doc.getElementsByClass("page_content").get(0).select("li"); // articles in format [month, all articles from that month, month, all....]
        int i = 0;
        ArrayList<Pair<String, String>> articles = new ArrayList<>(20);
        for(Element el : perMonth) {
            String title = el.select("strong").text().replaceAll(":", "");
            el.select("strong").remove();
            TextNode tn = el.textNodes().get(0);
            tn.text(tn.text().replaceAll(":", ""));
            String content = el.html();

            articles.add(new Pair<>(title, content));
            if(i++ == 8) break;
        }
        this.articles = articles;
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
            RemoteViews remoteViews = updateCurrentWidget(context, appWidgetId);
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        }
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.news_widget);
        //views.setTextViewText(R.id.appwidget_text, widgetText);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private RemoteViews updateCurrentWidget(Context context, int appWidgetId) {
        RemoteViews remoteViews;

        remoteViews = new RemoteViews(context.getPackageName(), R.layout.news_widget);
        remoteViews.setTextViewText(R.id.news_title1, articles.get(0).first);
        remoteViews.setTextViewText(R.id.news_title2, articles.get(1).first);
        remoteViews.setTextViewText(R.id.news_title3, articles.get(2).first);
        //remoteViews.setTextViewText(R.id.news_title4, articles.get(3).first);

        remoteViews.setTextViewText(R.id.news_content1, articles.get(0).second);
        remoteViews.setTextViewText(R.id.news_content2, articles.get(1).second);
        remoteViews.setTextViewText(R.id.news_content3, articles.get(2).second);
        //remoteViews.setTextViewText(R.id.news_content4, articles.get(3).second);

        return remoteViews;
    }
}
