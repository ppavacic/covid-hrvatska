package com.ppavacic.covidhr.Common;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class HtmlResponse implements com.android.volley.Response.Listener<String> {
    final private Method method;
    final private Object methodsObj;
    public HtmlResponse(Object methodsObjj, Method methodd) { //pass class and method which ot execute
        this.methodsObj = methodsObjj;
        this.method = methodd;
    }

    @Override
    public void onResponse(String response) {
        try {
            method.invoke(methodsObj, response);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
