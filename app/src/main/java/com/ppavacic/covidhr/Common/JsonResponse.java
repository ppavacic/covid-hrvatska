package com.ppavacic.covidhr.Common;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JsonResponse implements com.android.volley.Response.Listener<String> {
    final private Method method;
    final private Object methodsObj;
    public JsonResponse(Object methodsObjj, Method methodd) { //pass class and method which ot execute
        this.methodsObj = methodsObjj;
        this.method = methodd;
    }

    @Override
    public void onResponse(String response) {
        try {
            JSONArray array = new JSONArray(response);
            method.invoke(methodsObj, array);

        } catch (JSONException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            e.getTargetException().printStackTrace();
        }
    }
}
