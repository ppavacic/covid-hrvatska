package com.ppavacic.covidhr.Common;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.ppavacic.covidhr.MainActivity;
import com.ppavacic.covidhr.R;

public class Notifier {
    public enum CHANNELS {
        NEWS(0) , GEO(1), OTHER(2); // have to be in order 1, 2, 3...

        private final int value;
        CHANNELS(int value) {
            this.value = value;
        }
        public static final String[] channelNames = { "Vijesti", "Lokacijske usluge", "Ostalo" };

        public String getName() {
            return channelNames[value];
        }
        public int getValue() { return value; }
    };
    private static Notifier instance;
    private final Context appContext;

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notifier(Context context) {
        appContext = context;
        NotificationManager nManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        for(int i = 0; i < 3; i++) { // i goes to the number of channels
            String channelStr = CHANNELS.channelNames[i];
            NotificationChannel nCh = new NotificationChannel(channelStr, channelStr, NotificationManager.IMPORTANCE_HIGH); //TODO: high is annoying
            nCh.enableLights(true);
            nCh.setLightColor(Color.GRAY);
            nCh.enableVibration(false);
            //nCh.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            nCh.setShowBadge(false);
            nManager.createNotificationChannel(nCh);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static synchronized Notifier getInstance(Context context) {
        if (instance == null) {
            instance = new Notifier(context);
        }
        return instance;
    }

    public static synchronized Notifier getInstance() {
        return instance;
    }

    //all navigation ids start with navigation_[banana, apple]
    public synchronized void notify(CHANNELS chan, String title, String text, Intent onClickIntent) {
        PendingIntent contentIntent = PendingIntent.getActivity(appContext, 0,
                onClickIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Notification not = new NotificationCompat.Builder(appContext, chan.getName())
                .setContentTitle(title)
                .setSmallIcon(R.drawable.app_icon)
                .setContentIntent(contentIntent)
                //.setContentText(text)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(text))
                .setAutoCancel(true)
                .build();
        NotificationManagerCompat mngr = NotificationManagerCompat.from(appContext);
        mngr.notify(chan.getName(), chan.getValue(), not);
        //nManager.notify(Integer.parseInt(String.valueOf(chan)), not);
    }

    public synchronized void notify(CHANNELS chan, String title, String text) {
        Notification not = new NotificationCompat.Builder(appContext, chan.getName())
                .setContentTitle(title)
                .setSmallIcon(R.drawable.app_icon)
                //.setContentText(text)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(text))
                .build();
        NotificationManagerCompat mngr = NotificationManagerCompat.from(appContext);
        mngr.notify(chan.getValue(), not);
        //nManager.notify(Integer.parseInt(String.valueOf(chan)), not);
    }
}
