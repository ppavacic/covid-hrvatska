package com.ppavacic.covidhr.Common;

import androidx.lifecycle.ViewModelProvider;

import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.ppavacic.covidhr.Models.SharedViewModel;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class LineChartXAxisValueFormatter extends IndexAxisValueFormatter {

    private ArrayList<Date> dates;

    public LineChartXAxisValueFormatter(ArrayList<Date> dates) {
        this.dates = dates;
    }

    @Override
    public String getFormattedValue(float value) {
        DateFormat dateTimeFormat = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
        int skipNDates = dates.size() - Constants.daysToShow;
        int index = Math.min(skipNDates + Math.round(value), dates.size() - 1);
        return dateTimeFormat.format(dates.get(index));
    }
}