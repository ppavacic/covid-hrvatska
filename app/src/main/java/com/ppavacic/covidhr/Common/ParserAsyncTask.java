package com.ppavacic.covidhr.Common;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ParserAsyncTask extends AsyncTask<Void, Void, String> {

    private final String url;
    private final Object object;
    private final Method method;
    public ParserAsyncTask(String url, Object methodsObjj, Method methodd) { // url for request, object, and objects method on which to execute
        this.url = url;
        this.object = methodsObjj;
        this.method = methodd;
    }
    @Override
    protected String doInBackground(Void... voids) {
        try {
            Document doc = Jsoup.connect(url).get();
            method.invoke(object, doc);
            return doc.title();
        } catch (IOException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }
}
