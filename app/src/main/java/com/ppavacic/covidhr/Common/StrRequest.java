package com.ppavacic.covidhr.Common;

import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

public class StrRequest extends StringRequest {
    public StrRequest(String url, Object obj, java.lang.reflect.Method methodToPass) {
        super(Request.Method.GET, url, new JsonResponse(obj, methodToPass),
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Notifier.getInstance().notify(
                                Notifier.CHANNELS.OTHER,
                                "Pogreška pristupa mreži!",
                                "Provjerite ako ste povezani na mrežu. Alternativno ovo može biti pogreška s koronavirus pristupnim sučeljem.");
                    }
                });

    }
}
