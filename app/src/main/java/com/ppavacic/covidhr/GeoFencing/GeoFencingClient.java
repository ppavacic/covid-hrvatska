package com.ppavacic.covidhr.GeoFencing;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.*;
import com.google.android.gms.location.GeofencingRequest;
import com.ppavacic.covidhr.Common.Notifier;

public class GeoFencingClient {
    private PendingIntent geoPendingIntent;
    private final Activity activity;
    private boolean gps_enabled = false, network_enabled = false;
    private final ArrayList<Geofence> geoFences = new ArrayList<Geofence>(1);;
    public static final int FENCE_INTENT_CODE = 678;

    private static final double[][] GEO_FENCES_DATA = {
            {45.6600403,16.8258617, 33000, 0}, //bjel
            {45.0807059,18.3543347, 100000, 1}, //brod
            {42.77367939015875, 17.861708647556785, 60000, 2}, //dubr //ok
            {45.8402941, 15.8942921, 30000, 3}, //grad zg
            {45.25104416159632, 13.657783123298367, 42000, 4}, //istra //ok
            {45.3708876, 14.8806589, 43000, 5}, //karlo
            {46.1102873, 16.6349743, 25000, 6}, //kopri
            {46.0677653,15.6913883, 25000, 7}, //krap zag
            {44.6147149, 15.3335016, 50000, 8}, //licko senjska
            {46.533788,16.543808, 25000, 9}, //medjimurska //ok
            {45.5072221, 18.2170056, 42000, 10}, //osj bar
            {45.3993268, 17.3751782, 40000, 11}, //pož
            {45.407217, 14.705275, 36000, 12}, //pgz //ok
            {43.6893836,16.1838483, 43000, 13}, //sib
            {45.3205169, 16.3978022, 45000, 14}, //sisacko mosl
            {43.4261548, 16.6136883, 60000, 15}, //split
            {46.1884005,16.1531574, 25000, 16}, //varažd
            {45.6029023, 17.6415966, 54000, 17}, //vir
            {45.0831301, 18.3996533, 40000, 18}, //vuk
            {44.0221102, 15.281596, 47000, 19}, //zadar
            //{} //zagrebacka ne dodati

    };


    @RequiresApi(api = Build.VERSION_CODES.O)
    public GeoFencingClient(Activity activity) {
        GeofencingClient geoFenceClient = LocationServices.getGeofencingClient(activity);
        for(int i = 0; i < GEO_FENCES_DATA.length; i++) {
            double []countyData = GEO_FENCES_DATA[i];
            geoFences.add(getGeofence(countyData[0], countyData[1], (int)countyData[2], (int) countyData[3]));
        }
        this.activity = activity;


        LocationManager lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if(!gps_enabled || !network_enabled) {
                Toast.makeText(activity, "GPS isključen!", Toast.LENGTH_SHORT).show();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            geoFenceClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnSuccessListener(activity, aVoid -> Toast.makeText(activity, "GPS obavještavanje ukljuceno!", Toast.LENGTH_SHORT).show())
                .addOnFailureListener(activity, e -> Toast.makeText(activity, "GPS obavještavanje isključeno!", Toast.LENGTH_LONG).show());
        }

    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        //ENTER used only for demo
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_EXIT /*| GeofencingRequest.INITIAL_TRIGGER_DWELL*/);
        builder.addGeofences(geoFences);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        if (geoPendingIntent != null) {
            return geoPendingIntent;
        }
        Intent intent = new Intent(activity, GeoFencingReceiver.class);
        geoPendingIntent = PendingIntent.getBroadcast(activity, FENCE_INTENT_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return geoPendingIntent;
    }

    private Geofence getGeofence(double lat, double lon, int radiusM, int countyId) {
        //Notifier.getInstance().notify(Notifier.CHANNELS.OTHER, "" + countyId, "Creating fence lat: " +lat + "lon: " + lon + " radius " + radiusM + " countyID " + countyId);
        return new Geofence.Builder()
                .setRequestId(String.valueOf(countyId))
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setLoiteringDelay(500)
                .setCircularRegion(lat, lon, radiusM)
                .setNotificationResponsiveness(100)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT /*| Geofence.GEOFENCE_TRANSITION_DWELL*/)
                .build();
    }
}
