package com.ppavacic.covidhr.GeoFencing;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.*;

import com.google.android.gms.location.*;
import com.ppavacic.covidhr.Common.Notifier;
import com.ppavacic.covidhr.MainActivity;
import com.ppavacic.covidhr.R;

public class GeoFencingReceiver extends BroadcastReceiver {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        Notifier not2 = Notifier.getInstance(context);
        String[] countyNames3 = context.getResources().getStringArray(R.array.county_array_3);
        String[] countyNames2 = context.getResources().getStringArray(R.array.county_array_2);

        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceStatusCodes
                    .getStatusCodeString(geofencingEvent.getErrorCode());
            not2.notify(Notifier.CHANNELS.OTHER, "Pogreška lokacijskih usluga", "Pošaljite sliku zaslona sa ovim kodom: " + errorMessage);
            return;
        }
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

        if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL /*|| geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER*/) { //ENTER used only for demo
            int countyInd = Integer.parseInt(triggeringGeofences.get(0).getRequestId());
            String msg;

            if(countyInd == 3) msg = "Pročitajte najnovije vijesti iz " + countyNames2[countyInd]; //special case for city of zagreb
            else msg = "Pročitajte najnovije vijesti iz " + countyNames2[countyInd] + " županije!";

            Intent i = new Intent(context, MainActivity.class);
            i.putExtra("navigate_id_fragment", R.id.navigation_news);
            i.putExtra("news_county", countyInd);
            not2.notify(Notifier.CHANNELS.NEWS, "Dobrodošli u " + countyNames3[countyInd],  msg, i);

            for(Geofence tFence : triggeringGeofences) {

            }
        } else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            //not2.notify(Notifier.CHANNELS.OTHER, "Exit", "Izasao iz: " + triggeringGeofences.get(0).getRequestId());
        } else {
            System.out.println("Geofencing warning: " + "unhandled transition");
        }

    }
}
