## Crovid Hrvatska

Student project for mobile app development uni project.  
Coronavirus information app for Croatia.

## Features

### Quick overview of country data, and per-county data

Get country data and your county data:  
![Country and county data](https://gitlab.com/ppavacic/res/-/raw/main/Crovid/0-overview.jpg)

#### Geofencing

Automatically switch county to the relevant one as you travel. Get reminder to check news when entering a new country.  
Low power consumption, handled by Geofencing API, notifications appears even when the app is not running.  
To prevent notification spamming, notification appears only when user is in county for a few minutes.

![Geofencing](https://gitlab.com/ppavacic/res/-/raw/main/Crovid/1-gefencing.jpg)

#### Detailed per county data preview

Data is displayed on a graph, can be zoomed in, zoomed out so that the user can see both trends, and number of cases on any single day.  
![Display data](https://gitlab.com/ppavacic/res/-/raw/main/Crovid/3-stats.jpg)

#### Per county news

You can see relevant news in your own county. And check news in other counties.

![County news](https://gitlab.com/ppavacic/res/-/raw/main/Crovid/4-news-county.jpg)

#### Map with mass vaccination sites, testing sites and way to contact them

All the objects on the map are real objects where one can get vaccinated or tested for coronavirus.  
Map provides additional information such as e-mail, phone and open navigation to location.  
Contact a relevant site with only 2 clicks via phone call and E-mail. Easily navigate to it with a few clicks.  
Objects can provide additional data such as working hours, which entrance to use or other information as shown in image bellow.

![Covid site](https://gitlab.com/ppavacic/res/-/raw/main/Crovid/5-map-mass-vaccination.jpg)

#### Widget with news about user's county

Get information relevant for your country on your home screen.

![widget](https://gitlab.com/ppavacic/res/-/raw/main/Crovid/6-widget.jpg)
